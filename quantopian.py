"""
Quantrum: Следование по пересечениям разных периодов RSI
13x65
14x70
20x100

long
long&short
"""

import talib


# Setup our variables
def initialize(context):
    # Indices: 'DIA', 'QQQ', 'SPY', 'IWM'
    # Sectors: 'XLY', 'XLK', 'XLI', 'XLB', 'XLE', 'XLP', 'XLV', 'XLU', 'XLF', 'XLRE'
    context.stocks = symbols('SPY')
    context.stocks = symbols('XLY', 'XLK', 'XLI', 'XLB', 'XLE', 'XLP', 'XLV', 'XLU', 'XLF', 'XLRE')

    context.RSI_PERIOD_FAST, context.RSI_PERIOD_SLOW = 14, 70  # RSI fast/slow periods
    context.RSI_HOLD_DEVIATION = 10  # hold range
    context.DIRECTION = 'long' # long / short / long&short
    context.SMA_FILTER = False

    schedule_function(rsi_cross_follow, date_rules.every_day(), time_rules.market_open())

def rsi_cross_follow(context, data):
    """
    Стратегия на пересечении RSI разных периодов.
    """
    can_trade = data.can_trade(context.stocks)

    # Load historical data for the stocks
    prices = data.history(context.stocks, 'price', 500, '1d')

    trades = {
        'long': [],
        'short': []
    }
    hold = {
        'long': [],
        'short': []
    }

    # Loop through our list of stocks
    for stock in context.stocks:
        # skip stocks
        if not can_trade[stock]:
            continue

        # calculate RSI
        arr_rsi_fast = talib.RSI(prices[stock], timeperiod=context.RSI_PERIOD_FAST)
        rsi_fast = arr_rsi_fast[-2]
        rsi_fast_prev = arr_rsi_fast[-3]
        arr_rsi_slow = talib.RSI(prices[stock], timeperiod=context.RSI_PERIOD_SLOW)
        rsi_slow = arr_rsi_slow[-2]
        rsi_slow_prev = arr_rsi_slow[-3]

        # signal to open on enter
        long_allow = rsi_fast_prev < rsi_slow_prev and rsi_fast > rsi_slow and rsi_slow > 50
        short_allow = rsi_fast_prev > rsi_slow_prev and rsi_fast < rsi_slow and rsi_slow < 50

        # signal to hold
        long_hold = rsi_fast > rsi_slow - context.RSI_HOLD_DEVIATION
        short_hold = rsi_fast < rsi_slow + context.RSI_HOLD_DEVIATION

        # filter signals by sma200
        if context.SMA_FILTER:
            sma_short = talib.SMA(prices[stock], timeperiod=20)
            sma_long = talib.SMA(prices[stock], timeperiod=200)
            # filter open
            long_allow = long_allow and sma_short[-1] > sma_long[-1]
            short_allow = short_allow and sma_short[-1] < sma_long[-1]
            # filter hold
            long_hold = long_hold and sma_short[-1] > sma_long[-1]
            short_hold = short_hold and sma_short[-1] < sma_long[-1]

        if long_allow:
            trades['long'].append(stock)
        elif short_allow:
            trades['short'].append(stock)
        else:
            #log.warn('No signal for {0}'.format(stock.symbol))
            pass

        # hold only opened positions
        if long_hold and context.portfolio.positions[stock].amount > 0:
            hold['long'].append(stock)
        elif short_hold and context.portfolio.positions[stock].amount < 0:
            hold['short'].append(stock)

        #print(stock.symbol, long_allow, stock in trades['short'], short_hold, stock in hold['short'], context.portfolio.positions[stock].amount)

        if len(context.stocks) < 3:
            record(**{'RSI_SLOW_' + stock.symbol: rsi_slow})
            record(**{'RSI_FAST_' + stock.symbol: rsi_fast})
            #record(**{'PRICE_' + stock.symbol: current})
            #record(**{'SMA200_' + stock.symbol: sma[-1]})
            #record(**{'SMA_RSI_' + stock.symbol: sma_rsi[-2]})

    # open positions for the whole capital
    total_long = len(set(trades['long'] + hold['long']))
    total_short = len(set(trades['short'] + hold['short']))
    coef_long = 1./(total_long if total_long else 1)  # long is positive
    coef_short = 1./(total_short if total_short else 1)  # short is negative

    # disable direction
    if context.DIRECTION == 'long':
        # only long
        trades['short'] = []
        hold['short'] = []
    elif context.DIRECTION == 'short':
        # only short
        trades['long'] = []
        hold['long'] = []

    for stock in context.stocks:
        # buy to long stock
        if stock in trades['long']:
            order_target_percent(stock, coef_long)
            print('long', stock.symbol, "{0:.2f} x {1:.2f}".format(rsi_fast, rsi_slow))

        # sell to short stock
        elif stock in trades['short']:
            # negative percent
            order_target_percent(stock, -1 * coef_short)
            print('short', stock.symbol, "{0:.0f} x {1:.2f}".format(rsi_fast, rsi_slow))

        # close position
        elif (context.portfolio.positions[stock].amount
              and stock not in hold['long'] and stock not in hold['short']):
            order_target_percent(stock, 0)
            print('close', stock.symbol, "{0:.2f} x {1:.2f}".format(rsi_fast, rsi_slow))

        if len(context.stocks) < 3:
            record(**{stock.symbol: context.portfolio.positions[stock].amount})


